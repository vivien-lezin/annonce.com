<?php

namespace App\Form;

use App\Entity\Ad;
use App\Form\ImageType;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdNewType extends ApplicationType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',          TextType::class , $this         ->getConfiguration('Titre',                 'Titre de l\'annonce'))
            ->add('slug',           TextType::class , $this         ->getConfiguration('Adresse Web',           'Url de l\'annonce (automatique)' , ['required' => false]))
            ->add('coverImage',     UrlType::class , $this          ->getConfiguration('Image de couverture',   'Donner l\'adresse Url de l\'image de couverture'))
            ->add('introduction',   TextType::class , $this         ->getConfiguration('Introduction',          'Donner une description courte'))
            ->add('content',        TextareaType::class , $this     ->getConfiguration('Description détaillé',  'Donner une description de l\'annonce détaillé'))
            ->add('rooms',          IntegerType::class , $this      ->getConfiguration('Nombres de chambres',   'Le nombre de chambres disponible'))
            ->add('price',          MoneyType::class , $this        ->getConfiguration('Prix par nuit',         'Indiquer le prix par nuit'))
            ->add('images',         CollectionType::class,          
            [
                'entry_type'    => ImageType::class,
                'allow_add'     =>  true,
                'allow_delete'  => true
            ]
            )
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
