<?php

namespace App\Form;

use App\Entity\Ad;
use App\Entity\User;
use App\Entity\Booking;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Form\dataTransformer\FrenchToDateTimeTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AdminBookingType extends ApplicationType
{
    private $transformer;

    public function __construct(FrenchToDateTimeTransformer $transformer){
        $this->transformer = $transformer;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', TextType::class, $this->getConfiguration("Date d'arrivée","La date à laquelle vous comptez arriver"))
            ->add('endDate', TextType::class, $this->getConfiguration("Date de depart","La date à laquelle vous comptez parter"))
            ->add('comment',TextareaType::class, $this->getConfiguration("Commentaire","Le commentaire de la réservation"))
            ->add('booker',EntityType::class, $this->getConfiguration('Visiteur', 'Visiteur qui a réservé', [
                    'class' => User::class,
                    'choice_label' => function($user){
                        return $user -> getFirstName()." ".strtoupper($user->getLastName());
                    }
            ]))
            ->add('ad', EntityType::class, $this->getConfiguration('Annonce', 'Annonce de la réservation',[
                'class' => Ad::class,
                'choice_label' => 'title'
            ]))
        ;
        $builder ->get('startDate')->addModelTransformer($this->transformer);
        $builder ->get('endDate')->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'validation_groups' => [
                'Default'
            ]
        ]);
    }
}
