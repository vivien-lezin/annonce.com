<?php

namespace App\Controller;

use App\Repository\AdRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// On utilise la class AbstractController pour l'utiliser dans la nouvelle classe AbstractController 

class HomeController extends AbstractController {



    /**
     * Permet d'afficher la page d'accueil
     *
     * @Route("/",name="homepage")
     * @param AdRepository $adRepo
     * @return Reponse
     */
    public function home(AdRepository $adRepo, UserRepository $userRepo)
    {
        return $this->render(
            'home.html.twig',
            [
                'ads'=>$adRepo->findBestAds(3),
                'users'=>$userRepo->findBestUsers(2)
            ]
            );
    }

}

?>
