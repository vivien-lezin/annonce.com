<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\Image;
use App\Form\AdNewType;
use App\Repository\AdRepository;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdController extends AbstractController
{
    /**
     * @Route("/ads/{page<\d+>?1}", name="ads_index")
     */
    public function index(AdRepository $repo, PaginationService $pagination, $page)
    {

        $pagination ->setEntityClass(Ad::class)
                    ->setPage($page)
                    ->setLimit(12);

        $ads = $repo->findAll();

        return $this->render('ad/index.html.twig', [
            'ads' => $ads,
            'pagination' => $pagination
        ]);
    
    }


    /**
     * Permet de créé une annonce
     *
     * 
     * 
     * @Route("ads/new", name="ads_create")
     * 
     * @IsGranted("ROLE_USER")
     * 
     */
    public function create(Request $request, EntityManagerInterface $manager){
        $ad = new Ad();

        $form = $this   ->createForm(AdNewType::class,$ad);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            foreach($ad->getImages() as $image){
                $image->setAd($ad);
                $manager -> persist($image);
            }

            $ad->setAuthor($this->getUser());

            $manager->persist($ad);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été enregistrée !"
            );

            return $this ->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }
        

        return $this -> render('ad/new.html.twig',[
            'form' => $form->createView()
        ]);

    }
/**
 * Permet d'afficher le formulaire d'édition
 * 
 * @Route("ads/{slug}/edit", name="ads_edit")
 *
 * @Security("is_granted('ROLE_USER') and user === ad.getAuthor()", message="Cette annonce ne vout appartient pas, vous ne pouvez pas la modifier")
 * 
 */
    public function edit(Ad $ad, Request $request, EntityManagerInterface $manager){

        
        $form = $this   ->createForm(AdNewType::class,$ad);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            foreach($ad->getImages() as $image){
                $image->setAd($ad);
                $manager -> persist($image);
            }
            $manager->persist($ad);
            $manager->flush();

            $this->addFlash(
                'success',
                "L'annonce <strong>{$ad->getTitle()}</strong> a bien été modifié !"
            );

            return $this ->redirectToRoute('ads_show', [
                'slug' => $ad->getSlug()
            ]);
        }

        return $this-> render('ad/edit.html.twig',[
            'form' => $form->createView(), 
            'ad' => $ad
        ]);
    }

    /**
     * Permet d'afficher une seule annonce
     *
     * @Route("/ads/{slug}", name="ads_show")
     */
    public function show(Ad $ad){
        return $this -> render('ad/show.html.twig', [
            'ad' => $ad
        ]);
    
    
    }

    /**
     * Permet de suprimer une annonce
     * 
     * @Route("/ads/{slug}/delete", name="ads_delete")
     * 
     * @Security("is_granted('ROLE_USER') and user == ad.getAuthor()" , message="Vous n'avez pas le droit d'accéder à cette ressource")
     *
     * 
     * @param Ad $ad
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(Ad $ad, EntityManagerInterface $manager){

        $manager->remove($ad);
        $manager->flush();

        $this->addFlash(
            'success', "L'annonce <strong>{$ad->getTitle()}</strong> a bien été supprimée !"
        );

        return $this->redirectToRoute("ads_index");
    }

}
