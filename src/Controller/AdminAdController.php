<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Form\AdNewType;
use App\Repository\AdRepository;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminAdController extends AbstractController
{
    /**
     * @Route("/admin/ads/{page<\d+>?1}", name="admin_ads_index")
     */
    public function index(AdRepository $repo, $page, PaginationService $pagination)
    {

        $pagination ->setEntityClass(Ad::class)
                    ->setPage($page);


        return $this->render('admin/ad/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Permet d'afficher l'édition d'un utilisateur
     * 
     * @Route("/admin/ads/{id}/edit", name="admin_ad_edit")
     */
    public function edit(Ad $ad, Request $request, EntityManagerInterface $manager)
    {
        $form= $this->createForm(AdNewType::class, $ad);

        $form ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){


            $manager->flush();

            $this->addFlash(
                'success',
                "L'annonce {$ad->getTitle()} a bien été modifiée"
            );
            
            return $this->redirectToRoute("admin_ads_index");
            
        }
        return  $this->render('admin/ad/edit.html.twig',[
            'form' => $form->createView(),
            'ad' => $ad
        ]);

    }

    /**
     * Permet de supprimer un utilisateurs
     * 
     * @Route("/admin/ads/{id}/delete", name="admin_ad_delete")
     */
    public function delete(Ad $ad, EntityManagerInterface $manager)
    {
        $manager->remove($ad);
        $manager->flush();

        $this->addFlash(
            'success',
            'L\'annonce a bien été supprimer'
        );

        return $this->redirectToRoute("admin_ads_index");
    }

}
