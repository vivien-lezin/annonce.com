<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminUserController extends AbstractController
{
    /**
     * Permet d'afficher la gestion des utilisateurs
     * 
     * @Route("/admin/users", name="admin_users_index")
     */
    public function index(UserRepository $repo)
    {

        return $this->render('admin/user/index.html.twig', [
            'users' => $repo->findAll(),
        ]);
    }

    /**
     * Permet d'afficher l'édition d'un utilisateur
     * 
     * @Route("/admin/users/{id}/edit", name="admin_user_edit")
     */
    public function edit(User $user, Request $request, EntityManagerInterface $manager)
    {
        $form= $this->createForm(AccountType::class, $user);

        $form ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){


            $manager->flush();

            $this->addFlash(
                'success',
                "L'utilisateur {$user->getFullname()} a bien été modifiée"
            );
            
            return $this->redirectToRoute("admin_users_index");
            
        }
        return  $this->render('admin/user/edit.html.twig',[
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * Permet de supprimer un utilisateurs
     * 
     * @Route("/admin/users/{id}/delete", name="admin_user_delete")
     */
    public function delete(User $user, EntityManagerInterface $manager)
    {
        $manager->remove($user);
        $manager->flush();

        $this->addFlash(
            'success',
            'L\'utilisateur a bien été supprimer'
        );

        return $this->redirectToRoute("admin_users_index");
    }

}
