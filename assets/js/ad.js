
        $('#add-image').click(function(){
            // je récupère le numéro des futurs champs que je vais créé
            const index = +$('#widgets-counter').val();


            // Je récupère le prototype des entrées
            const tmpl = $('#ad_new_images').data('prototype').replace(/__name__/g, index);
            // J'injecte le code au sein du formulaire
            $('#ad_new_images').append(tmpl);
            $('#widgets-counter').val(index + 1)
            //Je gère le boutton delete
            handleDeleteButtons();
        })

        function handleDeleteButtons(){
            $('button[data-action="delete"]').click(function(){
                const target = this.dataset.target;

                $(target).remove();
            })
        }

        function updateCounter(){
            const count = $('#ad_new_images div.form-group').length;

            $('#widgets-counter').val(count);
        }
        
        updateCounter();

        handleDeleteButtons();